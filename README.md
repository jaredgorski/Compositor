# Compositor
Simple web-app for compositing RGBA colors


![](.media/compositor-demo.gif)

Link: [https://react-compositor.wedeploy.io/](https://react-compositor.wedeploy.io/)

### Develop:

`npm i`

`npm run build`

`npm start`


### Deploy on WeDeploy:
Prerequisite: [WeDeploy CLI](https://wedeploy.com/docs/intro/using-the-command-line/)

`npm run build`

`cd build`

`we deploy -p compositor`


### Explanation
This is a silly project bordering on uselessness, but fun nonetheless. Simply enter two RGBA colors you would like to composite together and enjoy the resulting RGB color as it fills the page background and displays in the yield. Click on the yield to search your new RGB color on Google.

Feel free to open issues, send pull-requests, or [ping the creator](mailto:jaredgorski6@gmail.com) with insults, kudos, or questions!
